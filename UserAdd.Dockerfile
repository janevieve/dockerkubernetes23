Example 1

Create a Docker file that uses a base image of CentOS 7 and create a user john and change to non-root privilege

# Base image is CentOS 7
FROM centos:7
# Add a new user "john" with user id 8877
RUN useradd -u 8877 john
# Change to non-root privilege
USER john