
Build an image from a Dockerfile

sudo docker build -t nonrootimage . # create custom image (nonrootimage)

Examples.

Create an image which has base image ubuntu and apache2 is to be installed on it and create an index.html file in current directory,  all the files from the current directory is to be copied to /var/www/html folder. Once the container is started it should run the apache service and also create one environment variable called "name" and it should have value "DEVOPS

#base Ubunutu image, manadatory command
FROM ubuntu
#ARGRUMENT it will not ask any question
ARG DEBIAN_FRONTEND=noninteractive
#Run command is used to add an instruction layer on top of base image layers
#Adding apt update instructions on top of ubuntu base image
RUN apt-get update
#Adding apache software on top of ubuntu base image
RUN apt-get -y install apache2
#ADD command will copy test.html(create test.html file) from local current directory 
#to docker image /var/www/html
ADD test.html /var/www/html
#ENTRYPOINT when a container is started with this image then it will first run 
# the command which written in front of EntryPoint
# below command starts apache in Foreground
ENTRYPOINT apachectl -D FOREGROUND
#ENV is to setup Enviornment variable
ENV name DEVOPS

Run the below command to build the image

Docker build . -t  img1   #Created the image from the above Dockerfile