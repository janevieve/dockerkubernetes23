#Example of COPY and ADD

#Base Image
FROM centos:7.4.1708
#Create a directory in the image mydata
RUN mkdir /mydata
# Copy myfiles (should be created under current folder ) in /mydata
COPY myfiles /mydata/myfiles  
# Copy myfile2 (should be created under current folder ) in /mydata
ADD myfile2 /mydata/myfile2  
# Download the file using ADD command and copy under /mydata
ADD https://mirrors.estointernet.in/apache/maven/maven-3/3.6.3/binaries/apache-maven-3.6.3-bin.tar.gz /mydata 
# ADD command will unzip the file and create under /mydata/maven dir
# wget https://mirrors.estointernet.in/apache/maven/maven-3/3.6.3/source/apache-maven-3.6.3-src.tar.gz
ADD apache-maven-3.6.3-src.tar.gz /mydata/maven


# CMD and ENTRYPOINT

#base image 
FROM ubuntu
# When a container get started from this image
# it will print Hello World
# CMD execute the commands but if we pass command line argument
# then echo "Hello World" will not execute and command line argument
# command get executed
CMD echo "Hello World"


    docker build . -t  img1   #Created the image from above Dockerfile

    docker run -it img1 # it will return Hello World

    docker run -it img1 echo "Hello India" # it will overwrite the CMD and Print Hello India

EntryPoint Example


#base image 
FROM ubuntu
# When a container get started from this image
# it will print Hello World
# ENTRYPOINT execute the commands once container get started
# it will not consider the command line arguments
ENTRYPOINT echo "Hello World"
docker build . -t  img1   #Created the image from above Dockerfile

docker run -it img1 # it will return Hello World

docker run -it img1 echo "Hello India" # it will not overwrite the ENTRYPOINT and Print Hello World 



Note:- if the file name is not Dockerfile

docker build . -f abc -t img8 # abc is the file name which represents the dockerfile contents